import pygame
import random
from entities.player import Player
from config import SCREEN_WIDTH, SCREEN_HEIGHT, PLATFORM_SPEED


class Platform(pygame.sprite.Sprite):
    '''
    Moves and checks collisions on platform
    Refactor: make different constructors for different platforms
    '''
    def __init__(self, start_platform=False, start_height=0, first_generation=False):
        super(Platform, self).__init__()
        self.speed = PLATFORM_SPEED
        self.image = pygame.image.load("assets\platform\Viga1.png").convert()
        if start_platform == True:
            self.image = pygame.transform.scale(self.image, (SCREEN_WIDTH, 20))
            self.rect = self.image.get_rect(
                center=(
                    SCREEN_WIDTH / 2, SCREEN_HEIGHT - 20 / 2
                )
            )
        elif first_generation:
            self.image = pygame.transform.scale(self.image, (random.randint(100, int(SCREEN_WIDTH/2)), 20))
            self.rect = self.image.get_rect(
                center=(
                    random.randint(0, SCREEN_WIDTH), SCREEN_HEIGHT - start_height
                )
            )
        else:
            self.image = pygame.transform.scale(self.image, (random.randint(100, int(SCREEN_WIDTH/3)), 20))
            self.rect = self.image.get_rect(
                center=(
                    random.randint(0, SCREEN_WIDTH), -20
                )
            )


    def update(self, speed_update):
        self._move(speed_update)

    def _move(self, speed_update):
        '''
        Moves cloud based on speed in config.py
        :param speed_update:
        :return:
        '''
        self.rect.move_ip(0, self.speed+speed_update)

    def _is_collision(self, player: Player):
        '''
        Simple collision check if bottom point of player sprite is inside platform
        :param player:
        :return:
        '''
        if player.rect.bottom >= self.rect.top and player.rect.bottom <= self.rect.bottom and player.rect.right >= self.rect.left and player.rect.left <= self.rect.right:
            player.collision = True
            player.rect.bottom = self.rect.top


