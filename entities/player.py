# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
import pygame
import math
from config import SCREEN_WIDTH, SCREEN_HEIGHT, PLAYER_ACCELERATION, PLAYER_MAX_ACCELERATION, FRICTION_FORCE, GRAVITY_FORCE, PLAYER_MAX_GRAVITY_ACCELERATION, PLAYER_JUMP_FORCE
from types import SimpleNamespace
from pygame.locals import (
    K_UP,
    K_LEFT,
    K_RIGHT,
)


class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.image = pygame.image.load("assets\cowboy\Cowboy4_idle without gun_3.png").convert()
        self.rect = self.image.get_rect(
            center=(
                SCREEN_WIDTH/2, SCREEN_HEIGHT/2
            )
        )
        self.acceleration = SimpleNamespace(x=0, y=0)
        self.direction_right = True
        self.collision = False
        self._load_images()

    def _load_images(self):
        '''
        Loads all images for this player to make animations
        :return: None
        '''
        self.index=0
        self.idle_images = [pygame.image.load(f"assets\cowboy\Cowboy4_idle without gun_{i}.png") for i in range(3)]
        self.walk_images_right = [pygame.image.load(f"assets\cowboy\Cowboy4_walk without gun_{i}.png") for i in range(3)]
        self.walk_images_left = [pygame.transform.flip(image,1,0) for image in self.walk_images_right]

    def _update_animation(self):
        '''
        Updates animations checks for direction of player and his acceleration
        :return: None
        '''
        self.index += 0.2
        if self.direction_right and self.acceleration.x > 0.1:
            if self.index >= len(self.walk_images_right):
                self.index = 0
            self.image = self.walk_images_right[math.floor(self.index)]
        elif not self.direction_right and self.acceleration.x < -0.1:
            if self.index >= len(self.walk_images_left):
                self.index = 0
            self.image = self.walk_images_left[math.floor(self.index)]
        else:
            if self.index >= len(self.idle_images):
                self.index = 0
            self.image = self.idle_images[math.floor(self.index)]


    # Move the sprite based on user keypresses
    def update(self, pressed_keys):
        '''
        Checks for user key preeses and performs all updates on sprite
        :param pressed_keys:
        :return:
        '''
        self._move()
        if pressed_keys[K_UP] and self.collision == True:
            self.acceleration.y = PLAYER_JUMP_FORCE
        if pressed_keys[K_LEFT]:
            self.direction_right=False
            self._accelerate()
        if pressed_keys[K_RIGHT]:
            self.direction_right=True
            self._accelerate()
        self._update_animation()
        self._keep_in_bounds()
        self.collision = False


    def _accelerate(self):
        '''
        Accelerates player when user presses move buttons, constraints player from going
        beyond max_acceleration.
        Note: Acceleration raises slowly due to frame constraint and low player_acceleration value
        :return: None
        '''
        if self.direction_right == True:
            self.acceleration.x += PLAYER_ACCELERATION
        else:
            self.acceleration.x -= PLAYER_ACCELERATION

        # Make sure acceleration is not higher than max_acceleration
        if self.acceleration.x > PLAYER_MAX_ACCELERATION:
            self.acceleration.x = PLAYER_MAX_ACCELERATION

    def _move(self):
        '''
        Applies gravity, friction to player and moves it
        :return:
        '''
        # if acceleration is above not 0 move player
        if abs(self.acceleration.x) > FRICTION_FORCE:
            self.rect.move_ip(self.acceleration.x, self.acceleration.y)

            # Apply friction force
            if self.direction_right == True:
                self.acceleration.x -= FRICTION_FORCE
            else:
                self.acceleration.x += FRICTION_FORCE

        self._apply_gravity()
        self.rect.move_ip(0, self.acceleration.y)

    def _keep_in_bounds(self):
        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

    def _apply_gravity(self):
        # Apply Gravity
        if self.collision == True:
            self.acceleration.y =0
        if self.acceleration.y < PLAYER_MAX_GRAVITY_ACCELERATION:
            self.acceleration.y += GRAVITY_FORCE



