## How to run
```
git clone <this repository>
pip install -r requirements.txt
python game.py
```

## Controls
```
up-arrow: Jump
left-arrow: go left
right-arrow: go right
esc-button: quit
```


## Overview
![](readme_images/game.gif)
