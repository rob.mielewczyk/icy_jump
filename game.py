# Import and initialize the pygame library
import pygame
from time import sleep
from config import SCREEN_HEIGHT, SCREEN_WIDTH, PLATFORM_DISTANCE, PLATFORM_SPEED, PLATFORM_SPEED_UPDATE
from entities.player import Player
from entities.platform import Platform
from entities.background import Background
from pygame.locals import (
    K_ESCAPE,
    KEYDOWN,
)

class Game():
    def __init__(self):
        '''
        Initializes game sets up windows size and loads spirtes
        '''
        pygame.init()
        self.points = 0
        self.clock = pygame.time.Clock()

        # Set up the drawing window
        self.screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

        # Instantiate player. Right now, this is just a rectangle.
        self.player = Player()
        self.background = Background('assets/background/airadventurelevel4.png', [0, 0])
        self.platforms = [Platform(first_generation=True, start_height=i*PLATFORM_DISTANCE) for i in range(8)]
        self.platforms.insert(0, Platform(start_platform=True))
        self.game_speed = PLATFORM_SPEED
        self.game_loop()


    # Run until the user asks to quit
    def game_loop(self):
        '''
        Main game loop this function shows all functions that are going to be invoked
        :return: None
        '''
        running = True
        while running:
            running = self._check_quit_events()
            self._update()

            # Fill the background with white
            self.screen.fill((0, 255, 255))

            self._display()

    def _update(self):
        '''
        This is where game logic happens all collisions and changes in sprites are updated here
        :return: None
        '''
        # update game speed
        self.game_speed+=PLATFORM_SPEED_UPDATE

        # Get the set of keys pressed and check for user input
        pressed_keys = pygame.key.get_pressed()

        # Update the player sprite based on user keypresses
        self.player.update(pressed_keys)
        for platform in self.platforms:
            platform.update(self.game_speed)
            platform._is_collision(self.player)

            # If platform is out of bounds remove it and add another
            if platform.rect.top > SCREEN_HEIGHT:
                platform.kill()
                self.platforms.remove(platform)
                self.platforms.append(Platform())


    def _display(self):
        '''
        Updates screen frame and puts all sprites on the frame (at their current settings) this is
        the last step in game_loop
        Note: order is important here so that we don't display background
        on top of other sprites
        Refactor: If we'd have more entities it would be better to put display logic inside them and
        preferably make an abstract class for them as well
        :return: None
        '''
        # Draw Background Firts
        self.screen.blit(self.background.image, self.background.rect)

        # Draw the entities on the screen
        self.screen.blit(self.player.image, self.player.rect)
        for platform in self.platforms:
            self.screen.blit(platform.image, platform.rect)

        self.check_gameover()
        self.show_points()
        # flip frame
        pygame.display.flip()
        self.clock.tick(30)


    def _check_quit_events(self):
        # Did the user click the window close button?
        for event in pygame.event.get():
            # Did the user hit a key?
            if event.type == KEYDOWN:
                # Was it the Escape key? If so, stop the loop.
                if event.key == K_ESCAPE:
                    return False

            if event.type == pygame.QUIT:
                return False
        return True

    def check_gameover(self):
        '''
        Renders gameover text if player touches bottom of the screen
        and restarts the game
        :return: None
        '''
        if self.player.rect.bottom >= SCREEN_HEIGHT:
            font = pygame.font.Font('freesansbold.ttf', 64)
            pointsFont = pygame.font.Font('freesansbold.ttf', 24)


            text = font.render('Game Over', True , (230,20,20))
            points = pointsFont.render(f'Points: {self.points}', True , (100,200,20))

            pointsRect = points.get_rect()
            textRect = text.get_rect()

            textRect.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2)
            pointsRect.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 + 80)
            self.screen.blit(text, textRect)
            self.screen.blit(points, pointsRect)
            pygame.display.flip()
            sleep(1)
            self.__init__()

    def show_points(self):
        self.points+=int(self.game_speed*10)
        font = pygame.font.Font('freesansbold.ttf', 18)
        text = font.render(f'Points {self.points}', True, (100, 200, 20))
        textRect = text.get_rect()
        textRect.center = (60, 20)
        self.screen.blit(text, textRect)


if __name__ == '__main__':
    Game()
    pygame.quit()





